//
//  AppDelegate.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.showLauchScreen()
 
        let email = KeyChainHelper.loadString(key: Constants.KeyChains.email)
        let senha = KeyChainHelper.loadString(key: Constants.KeyChains.password)
        let biometry = Global.shared.isBiometry() && PrefsHelper.isKeyExists(chave: Constants.Prefs.user_touch_id) && PrefsHelper.getBoolean(chave: Constants.Prefs.user_touch_id)
        if email != "" && senha != "" &&  biometry {
            let myContext = LAContext()
            myContext.localizedFallbackTitle = ""
            myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Toque no sensor para acessar seu aplicativo") { success, evaluateError in
                DispatchQueue.main.async {
                    if (success) {
                        self.initializeApplication(sucesso: true)
                    } else {
                        self.initializeApplication(sucesso: false)
                    }
                }
            }
        } else {
            self.initializeApplication(sucesso: true)
        }
 
        return true
    }

    func showLauchScreen() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "LaunchScreen", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "LaunchView")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    func initializeApplication(sucesso: Bool) {
        let email = KeyChainHelper.loadString(key: Constants.KeyChains.email)
        let senha = KeyChainHelper.loadString(key: Constants.KeyChains.password)
        if email != "" && senha != "" && sucesso {
            let empresaView = EmpresaRouter.loadModule()
            
            let navigator = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "EmpresaNavigator") as! UINavigationController
            //navigator.modalTransitionStyle = .flipHorizontal
            navigator.viewControllers = [empresaView]
            window?.rootViewController = navigator
            window?.makeKeyAndVisible()
        } else {
            let _ = KeyChainHelper.saveString(key: Constants.KeyChains.email, value: "")
            let _ = KeyChainHelper.saveString(key: Constants.KeyChains.password, value: "")
            PrefsHelper.removeKey(chave: Constants.Prefs.user_touch_id)
            
            let loginView = LoginRouter.loadModule()
            window?.rootViewController = loginView
            window?.makeKeyAndVisible()
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let _ = self.saveContext()
    }

    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "EmpresaModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                return false
            }
        }
        return true
    }
}

