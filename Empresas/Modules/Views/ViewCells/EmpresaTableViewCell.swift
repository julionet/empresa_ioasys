//
//  EmpresaTableViewCell.swift
//  Empresas
//
//  Created by MacOS on 28/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class EmpresaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func updateCell(empresa: Empresa) {
        self.nameLabel.text = empresa.enterprise_name
        self.typeLabel.text = empresa.enterprise_type?.enterprise_type_name
        self.countryLabel.text = empresa.country
        self.photoImage.image = self.getImageEmpresa(empresa: empresa)
    }
    
    func getImageEmpresa(empresa: Empresa) -> UIImage? {
        let imagem = ImagemDataManager.getById(id: Int32(empresa.id))
        if imagem == nil {
            if let photo = empresa.photo {
                self.downloadImage(id: empresa.id, url: photo)
                return nil
            } else {
                return UIImage(named: "imgDefault")
            }
        } else {
            if let image = imagem!.image {
                return UIImage(data: image)
            } else {
                return UIImage(named: "imgDefault")
            }
        }
    }
    
    func downloadImage(id: Int, url: String) {
        let url = NSURL(string: "\(Constants.Urls.api_imagem)\(url)")!
        let task = URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
            if (error == nil) {
                DispatchQueue.main.sync(execute: {
                    let retorno = ImagemDataManager.insert(id: Int32(id), image: data!)
                    if retorno {
                        self.photoImage.image = UIImage(data: data!)
                    } else {
                        self.photoImage.image = UIImage(named: "imgDefault")
                    }
                })
            } else {
                self.photoImage.image = UIImage(named: "imgDefault")
            }
        }
        task.resume()
    }
}
