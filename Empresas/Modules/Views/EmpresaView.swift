//
//  EmpresaView.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class EmpresaView: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pesquisaBarButtonItem: UIBarButtonItem!
    
    let searchBar = UISearchBar()
    var empresas = [Empresa]()
    var presenter: EmpresaViewToPresenterProtocol?
    var pesquisarButtonCopy: UIBarButtonItem?

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        
        self.searchBar.delegate = self
        
        self.showImageNavigationItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: Actions
    
    @IBAction func pesquisaClick(_ sender: UIBarButtonItem) {
        self.pesquisarButtonCopy = self.pesquisaBarButtonItem
        self.navigationItem.rightBarButtonItems?.removeAll()
        self.searchBar.sizeToFit()
        self.searchBar.placeholder = "Pesquisar"
        self.searchBar.text = ""
        self.searchBar.showsCancelButton = true
        self.navigationItem.titleView = self.searchBar
        self.searchBar.becomeFirstResponder()
        self.tableView.isHidden = false
    }
    
    func showImageNavigationItem() {
        let logo = UIImage(named: "logoNav")
        let imageView = UIImageView(image: logo)
        self.navigationItem.titleView = imageView
    }
}

// MARK: Protocols

extension EmpresaView: EmpresaPresenterToViewProtocol {
    
    func pesquisarSuccess(empresas: [Empresa]) {
        self.empresas = empresas
        self.tableView.reloadData()
    }
    
    func pesquisarFailed(message: String) {
        let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Table view delegate and data source

extension EmpresaView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empresas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellEmpresa", for: indexPath) as! EmpresaTableViewCell
        cell.updateCell(empresa: empresas[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.loadEmpresaDetalhe(empresa: self.empresas[indexPath.row], view: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98.0
    }
}

// MARK: SearchBar delegate

extension EmpresaView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{
            self.presenter?.pesquisarEmpresa(texto: searchText)
        } else {
            self.empresas = []
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.rightBarButtonItems?.append(self.pesquisarButtonCopy!)

        self.searchBar.showsCancelButton = false
        self.searchBar.endEditing(true)
        
        self.tableView.isHidden = true
        self.showImageNavigationItem()
        
        self.empresas = []
        self.tableView.reloadData()
    }
}
