//
//  DetalheView.swift
//  Empresas
//
//  Created by MacOS on 28/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class DetalheView: UIViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var empresa: Empresa?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationItem.title = empresa?.enterprise_name
        
        self.descriptionTextView.text = empresa?.description
        if let imagem = ImagemDataManager.getById(id: Int32(empresa!.id)) {
            self.photoImageView.image = UIImage(data: imagem.image!)
        } else {
            self.photoImageView.image = UIImage(named: "imgDefault")
        }
    }
}
