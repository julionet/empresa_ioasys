//
//  LoginView.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class LoginView: UITableViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var entrarButton: UIButton!
    
    var presenter: LoginViewToPresenterProtocol?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.delegate = self
        self.senhaTextField.delegate = self
        
        self.emailTextField.setBottomBorder()
        self.senhaTextField.setBottomBorder()
        
        self.emailTextField.leftViewMode = UITextField.ViewMode.always
        let imageViewEmail = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let imageEmail = UIImage(named: "icEmail")
        imageViewEmail.image = imageEmail
        self.emailTextField.leftView = imageViewEmail
        
        self.senhaTextField.leftViewMode = UITextField.ViewMode.always
        let imageViewSenha = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let imageSenha = UIImage(named: "icCadeado")
        imageViewSenha.image = imageSenha
        self.senhaTextField.leftView = imageViewSenha
        
        self.emailTextField.becomeFirstResponder()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: Actions
    
    @IBAction func entrarClick(_ sender: UIButton) {
        let alert = ProgressView.showProgress(texto: "Por favor aguarde...")
        self.present(alert, animated: true, completion: nil)
        
        let login = Login(email: self.emailTextField.text!, password: self.senhaTextField.text!)
        self.presenter?.executarLogin(login: login)
    }
    
    func loginAutomatico() {
        self.presenter?.loadEmpresa(view: self)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: Protocols

extension LoginView: LoginPresenterToViewProtocol {
    
    func executarSuccess(authentication: Authentication) {
        self.dismiss(animated: true) {
            let _ = KeyChainHelper.saveString(key: Constants.KeyChains.email, value: self.emailTextField.text!)
            let _ = KeyChainHelper.saveString(key: Constants.KeyChains.password, value: self.senhaTextField.text!)
            
            PrefsHelper.setString(valor: authentication.accessToken!, chave: Constants.Prefs.access_token)
            PrefsHelper.setString(valor: authentication.client!, chave: Constants.Prefs.client)
            PrefsHelper.setString(valor: authentication.uid!, chave: Constants.Prefs.uid)
            
            if Global.shared.isBiometry() && !PrefsHelper.isKeyExists(chave: Constants.Prefs.user_touch_id) {
                let confirmacaoTouchId = UIAlertController(title: "Confirmação", message: "Deseja utilizar biometria para acesso ao aplicativo?", preferredStyle: .alert)
                confirmacaoTouchId.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
                    PrefsHelper.setBoolean(valor: true, chave: Constants.Prefs.user_touch_id)
                    self.presenter?.loadEmpresa(view: self)
                }))
                confirmacaoTouchId.addAction(UIAlertAction(title: "Não", style: .cancel, handler: { (action: UIAlertAction!) in
                    PrefsHelper.setBoolean(valor: false, chave: Constants.Prefs.user_touch_id)
                    self.presenter?.loadEmpresa(view: self)
                }))
                self.present(confirmacaoTouchId, animated: true, completion: nil)
            } else {
                self.presenter?.loadEmpresa(view: self)
            }
        }
    }
    
    func executarFailed(message: String) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: Text field delegate

extension LoginView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (self.emailTextField == textField) {
            self.senhaTextField.becomeFirstResponder()
        } else if (senhaTextField == textField) {
            textField.resignFirstResponder()
            self.entrarClick(self.entrarButton)
        }
        return true
    }
    
}
