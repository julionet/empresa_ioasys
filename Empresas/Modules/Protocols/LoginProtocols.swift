//
//  LoginProtocols.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPresenterToViewProtocol: class {
    func executarSuccess(authentication: Authentication)
    func executarFailed(message: String)
}

protocol LoginInteractorToPresenterProtocol: class {
    func executarSuccess(authentication: Authentication)
    func executarFailed(message: String)
}

protocol LoginPresenterToInteractorProtocol: class {
    var presenter: LoginInteractorToPresenterProtocol? {get set}
    func executar(login: Login)
}

protocol LoginViewToPresenterProtocol: class {
    var view: LoginPresenterToViewProtocol? {get set}
    var interactor: LoginPresenterToInteractorProtocol? {get set}
    var router: LoginPresenterToRouterProtocol? {get set}
    func executarLogin(login: Login)
    func loadEmpresa(view: UIViewController)
}

protocol LoginPresenterToRouterProtocol: class {
    static func loadModule() -> UIViewController
    func loadEmpresa(view: UIViewController)
}
