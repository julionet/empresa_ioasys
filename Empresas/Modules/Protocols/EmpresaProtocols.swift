//
//  EmpresaProtocols.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

protocol EmpresaPresenterToViewProtocol: class {
    func pesquisarSuccess(empresas: [Empresa])
    func pesquisarFailed(message: String)
}

protocol EmpresaInteractorToPresenterProtocol: class {
    func pesquisarSuccess(empresas: [Empresa])
    func pesquisarFailed(message: String)
}

protocol EmpresaPresenterToInteractorProtocol: class {
    var presenter: EmpresaInteractorToPresenterProtocol? {get set}
    func pesquisarEmpresa(texto: String)
}

protocol EmpresaViewToPresenterProtocol: class {
    var view: EmpresaPresenterToViewProtocol? {get set}
    var interactor: EmpresaPresenterToInteractorProtocol? {get set}
    var router: EmpresaPresenterToRouterProtocol? {get set}
    func pesquisarEmpresa(texto: String)
    func loadEmpresaDetalhe(empresa: Empresa, view: UIViewController)
}

protocol EmpresaPresenterToRouterProtocol: class {
    static func loadModule() -> UIViewController
    func loadEmpresaDetalhe(empresa: Empresa, view: UIViewController)
}
