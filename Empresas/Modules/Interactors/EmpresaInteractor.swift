//
//  EmpresaInteractor.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class EmpresaInteractor: EmpresaPresenterToInteractorProtocol {
    
    var presenter: EmpresaInteractorToPresenterProtocol?
    
    func pesquisarEmpresa(texto: String) {
        EmpresaService.selecionarEmpresas(pesquisa: texto) { (empresas: [Empresa], error: NSError?) in
            if error != nil {
                self.presenter?.pesquisarFailed(message: (error?.localizedFailureReason)!)
            } else {
                self.presenter?.pesquisarSuccess(empresas: empresas)
            }
        }
    }
}
