//
//  LoginInteractor.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class LoginInteractor: LoginPresenterToInteractorProtocol {    

    var presenter: LoginInteractorToPresenterProtocol?

    func executar(login: Login) {
        if !NetworkHelper.isConnectedToNetwork() {
            self.presenter?.executarFailed(message: "Dispositivo com conexão instável ou sem acesso a internet.\nVerifique se sua conexão está disponível.\nPor favor, tente novamente.")
        } else if login.email == nil || login.email == "" {
            self.presenter?.executarFailed(message: "E-mail não informado!")
        } else if login.password == nil || login.password == "" {
            self.presenter?.executarFailed(message: "Senha não informada!")
        } else {
            LoginService.executar(login: login) { (authentication: Authentication?, error: NSError?) in
                if error != nil {
                    self.presenter?.executarFailed(message: (error?.localizedFailureReason)!)
                } else {
                    self.presenter?.executarSuccess(authentication: authentication!)
                }
            }
        }
    }

}
