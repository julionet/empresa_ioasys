//
//  EmpresaPresenter.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class EmpresaPresenter: EmpresaViewToPresenterProtocol {

    var view: EmpresaPresenterToViewProtocol?
    var interactor: EmpresaPresenterToInteractorProtocol?
    var router: EmpresaPresenterToRouterProtocol?
    
    func pesquisarEmpresa(texto: String) {
        self.interactor?.pesquisarEmpresa(texto: texto)
    }
    
    func loadEmpresaDetalhe(empresa: Empresa, view: UIViewController) {
        self.router?.loadEmpresaDetalhe(empresa: empresa, view: view)
    }
}

extension EmpresaPresenter: EmpresaInteractorToPresenterProtocol {
    
    func pesquisarSuccess(empresas: [Empresa]) {
        self.view?.pesquisarSuccess(empresas: empresas)
    }
    
    func pesquisarFailed(message: String) {
        self.view?.pesquisarFailed(message: message)
    }
    
    func inserirImagemSuccess() {
        
    }
    
    func inserirImagemFailed() {
        
    }
}
