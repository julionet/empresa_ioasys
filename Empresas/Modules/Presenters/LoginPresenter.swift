//
//  LoginPresenter.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class LoginPresenter: LoginViewToPresenterProtocol {
    
    var view: LoginPresenterToViewProtocol?
    var interactor: LoginPresenterToInteractorProtocol?
    var router: LoginPresenterToRouterProtocol?
    
    func executarLogin(login: Login) {
        self.interactor?.executar(login: login)
    }
    
    func loadEmpresa(view: UIViewController) {
        self.router?.loadEmpresa(view: view)
    }
}

extension LoginPresenter: LoginInteractorToPresenterProtocol {
    
    func executarSuccess(authentication: Authentication) {
        self.view?.executarSuccess(authentication: authentication)
    }
    
    func executarFailed(message: String) {
        self.view?.executarFailed(message: message)
    }
    
}
