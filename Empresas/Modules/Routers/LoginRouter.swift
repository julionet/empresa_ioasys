//
//  LoginRouter.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter: LoginPresenterToRouterProtocol {
    
    static func loadModule() -> UIViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "LoginView") as? LoginView
        let presenter: LoginViewToPresenterProtocol & LoginInteractorToPresenterProtocol = LoginPresenter()
        let interactor: LoginPresenterToInteractorProtocol = LoginInteractor()
        let router: LoginPresenterToRouterProtocol = LoginRouter()
        
        view?.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view!
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func loadEmpresa(view: UIViewController) {
        let empresaView = EmpresaRouter.loadModule()
        
        let navigator = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "EmpresaNavigator") as! UINavigationController
        navigator.modalTransitionStyle = .flipHorizontal
        navigator.viewControllers = [empresaView]
        view.present(navigator, animated: true, completion: nil)
    }
}

