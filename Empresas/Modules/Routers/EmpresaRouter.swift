//
//  EmpresaRouter.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class EmpresaRouter: EmpresaPresenterToRouterProtocol {

    static func loadModule() -> UIViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "EmpresaView") as? EmpresaView
        let presenter: EmpresaViewToPresenterProtocol & EmpresaInteractorToPresenterProtocol = EmpresaPresenter()
        let interactor: EmpresaPresenterToInteractorProtocol = EmpresaInteractor()
        let router: EmpresaPresenterToRouterProtocol = EmpresaRouter()
        
        view?.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view! 
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func loadEmpresaDetalhe(empresa: Empresa, view: UIViewController) {
        let detalheView = EmpresaRouter.mainstoryboard.instantiateViewController(withIdentifier: "DetalheView") as! DetalheView
        detalheView.empresa = empresa
        //view.navigationController?.viewControllers.append(detalheView)
        view.navigationController?.pushViewController(detalheView, animated: true)
    }
}
