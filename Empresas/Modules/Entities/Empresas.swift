//
//  Empresas.swift
//  Empresas
//
//  Created by MacOS on 29/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class Empresas: Codable {
    var enterprises: [Empresa]!
}
