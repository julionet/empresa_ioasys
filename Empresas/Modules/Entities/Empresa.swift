//
//  Empresa.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class Empresa: Codable {
    var id: Int = 0
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool = false
    var enterprise_name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Int = 0
    var share_price: Int = 0
    var enterprise_type: EnterpriseType?
}

class EnterpriseType: Codable {
    var id: Int = 0
    var enterprise_type_name: String?
}
