//
//  EmpresaService.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class EmpresaService {
    
    typealias callback = (_ empresas: [Empresa], _ error: NSError?) -> Void
    
    class func selecionarEmpresas(pesquisa: String, callback: @escaping callback) {
        let config = URLSessionConfiguration.default
        let headers =  ["content-type" : "application/json","access-token" : PrefsHelper.getString(chave: "access_token"),"client" : PrefsHelper.getString(chave: "client"),"uid" : PrefsHelper.getString(chave: "uid")]
        config.httpAdditionalHeaders = headers
        let session = URLSession(configuration: config)
        let url = NSURL(string: Constants.Urls.api_empresa + pesquisa)
        let task = session.dataTask(with: url! as URL) {
            (data, response, error) in
            if (error != nil) {
                DispatchQueue.main.sync(execute: {
                    callback([], error as NSError?)
                })
            } else {
                if data?.count != 0 {
                    let retorno = try? JSONDecoder().decode(Empresas.self, from: data!)

                    var empresas = [Empresa]()
                    for empresa in (retorno?.enterprises)! {
                        empresas.append(empresa)
                    }

                    DispatchQueue.main.sync(execute: {
                        callback(empresas, nil)
                    })
                }
            }
        }
        task.resume()
    }
}
