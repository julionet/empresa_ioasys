//
//  LoginService.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class LoginService {
    
    typealias callback = (_ authentication: Authentication?, _ error: NSError?) -> Void
    
    class func executar(login: Login, callback: @escaping callback) {
        let config = URLSessionConfiguration.default
        let url = NSURL(string: Constants.Urls.api_login)!
        var request = URLRequest(url: url as URL as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONEncoder().encode(login)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.sync(execute: {
                    callback(nil, error as NSError?)
                })
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        let authentication = Authentication()
                        authentication.accessToken = httpResponse.allHeaderFields["access-token"] as? String
                        authentication.client = httpResponse.allHeaderFields["client"] as? String
                        authentication.uid = httpResponse.allHeaderFields["uid"] as? String
                        DispatchQueue.main.sync(execute: {
                            callback(authentication, nil)
                        })
                    } else if httpResponse.statusCode == 401 {
                        let erro = ErrorHelper.createErro(mensagem: "Falha na autenticação", detalhes: "Não foi possível autenticar as informações de acesso, documento ou senha incorretos!", status: httpResponse.statusCode)
                        DispatchQueue.main.sync(execute: {
                            callback(nil, erro)
                        })
                    } else {
                        let erro = ErrorHelper.createErro(mensagem: "Falha ao tentar acessar dados", detalhes: httpResponse.description, status: httpResponse.statusCode)
                        DispatchQueue.main.sync(execute: {
                            callback(nil, erro)
                        })
                    }
                }
            }
        }
        task.resume()
    }
}
