//
//  Constants.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

struct Constants {

    struct KeyChains {
        static let email = "email"
        static let password = "password"
    }
    
    struct Prefs {
        static let access_token = "access_token"
        static let client = "client"
        static let uid = "uid"
        static let user_touch_id = "user_touch_id"
    }
    
    struct Urls {
        static let api_login = "http://empresas.ioasys.com.br/api/v1/users/auth/sign_in"
        static let api_empresa = "http://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=7&name="
        static let api_imagem = "http://empresas.ioasys.com.br/"
    }
    
}
