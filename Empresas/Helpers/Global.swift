//
//  Global.swift
//  Empresas
//
//  Created by MacOS on 29/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import LocalAuthentication

final class Global {
    
    private init() { }
    
    static let shared = Global()
    
    var biometry: Bool? = nil
    
    func isBiometry() -> Bool {
        if biometry == nil {
            var authError: NSError?
            
            let myContext = LAContext()
            myContext.localizedFallbackTitle = ""
            biometry = myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError)
        }
        return biometry!
    }
}
