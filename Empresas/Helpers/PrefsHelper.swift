//
//  PrefsHelper.swift
//  Empresas
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class PrefsHelper {
    
    class func isKeyExists(chave: String) -> Bool {
        return UserDefaults.standard.object(forKey: chave) != nil
    }
    
    class func removeKey(chave: String) {
        UserDefaults.standard.removeObject(forKey: chave)
    }
    
    class func setString(valor: String, chave: String) {
        let prefs = UserDefaults.standard
        prefs.setValue(valor, forKey: chave)
        prefs.synchronize()
    }
    
    class func getString(chave: String) -> String {
        let prefs = UserDefaults.standard
        return prefs.string(forKey: chave)!
    }
    
    class func setBoolean(valor: Bool, chave: String) {
        let prefs = UserDefaults.standard
        prefs.setValue(valor, forKey: chave)
        prefs.synchronize()
    }
    
    class func getBoolean(chave: String) -> Bool {
        let prefs = UserDefaults.standard
        return prefs.bool(forKey: chave)
    }
}
